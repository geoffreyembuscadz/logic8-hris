<?php
namespace App\Http\Controllers;
use \Schema;
use \Input AS Input;
use \Validator AS Validator;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Route AS Route;

class CrudController extends Controller {

  public $auth = true;

  public $data = [];

  public $errors = [];

  public $input;

  public $list_columns = [];

  public $model;

  public $per_page = 10;

  public $request;

  public $rules = [ 'name' => 'required' ];

  public $table = '';

  public $table_columns;

  public $view_folder = '';

  public function __construct(){
    $this->request = new Request();

    if($this->auth){
      $this->middleware('auth');
    }

    if(!empty($this->table)){
      $this->table_columns = Schema::getColumnListing( $this->table );
    }

    if(empty($this->view_folder) && !empty($this->table)){
      $this->view_folder = $this->table;
    }

    foreach($this->table_columns AS $table_col){
      if( $this->list_columns ){
        foreach( $this->list_columns AS $select_list_col ){
          if( $select_list_col == $table_col ){
            $this->data[ 'thead' ][ $table_col ] = ucfirst(str_replace('_', ' ', $table_col));
          }
        }
      } else {
        $this->data[ 'thead' ][ $table_col ] = ucfirst(str_replace('_', ' ', $table_col));
      }
    }


    if(!empty($this->table) && empty($this->model)){
      $temp_table = str_replace( '_', ' ', $this->table );
      $temp_table = ucwords( $temp_table );
      $temp_table = str_replace( ' ', '', $temp_table );

      // from table name to model name convention
      $this->model = ( empty( $this->model ) ) ? $temp_table : $this->model;
      $this->model = 'App\Http\Models\\' . $this->model;
      $this->model = new $this->model();
    } else if(empty($this->table) && !empty($this->model)){
      $this->model = 'App\Http\Models\\' . $this->model;
      $this->model = new $this->model();
    } else if(!empty($this->table) && !empty($this->model)){
      $this->model = 'App\Http\Models\\' . $this->model;
      $this->model = new $this->model();
    }
  }

  public function addListData($data = []){
    return $data;
  }

  public function addFormData($id = null){
    $data = [];

    return $data;
  }

  public function preList($model){
    // Where Clauses and Join Clauses
    $model->whereRaw('deleted_at IS NOT NULL');

    return $model;
  }

  public function postList($model){
    // Select
    return $model;
  }

  public function getList(){
    $model = $this->model;
    $model = $this->preList($model);
    $model = $this->postList($model);

    return $model->get();
  }

  public function preStore($data = []){
    return $data;
  }

  public function postStore($id, $data = []){
    return $data;
  }

  public function store(Request $request){
    $errors = $this->validate($request, $this->rules);

        if( !$errors ){
            $data = [];

            $data['data'] = $request->all();

            $data = $this->preStore($data);

            $insert_data = $this->model;

            foreach($this->table_columns AS $column){
                foreach($data['data'] AS $col_index => $value){
                    if($col_index == $column){
                        $insert_data->{$column} = $value;
                    }
                }
            }

            $insert_data->created_by = 1;// to be commented out if auth is implemented

            $insert_data->save();

            $data = $this->postStore($insert_data->id, $data);

            $data['message'] = 'Record is successfully added.';
        }

        $data['errors'] = $errors ? $errors : [];

        return $errors ? $errors : $data;
  }

  public function preUpdate($id, $data = []){
    return $data;
  }

  public function postUpdate($id, $data = []){
    return $data;
  }

  public function update(Request $request, $id){
    $errors = $this->validate($request, $this->rules);

        if( !$errors ){
            $data = [];

            $data['data'] = $request->all();

            $data = $this->preUpdate($id, $data);

            $update_data = $this->model->where('id', $id);

            $update_queries = [];

            foreach($this->table_columns AS $column_table){
              foreach($data['data'] AS $column_index => $value){
                if($column_index == $column_table){
                  if(!empty($value)){
                    $update_queries[$column_table] = $value;
                    // $update_data->{$column_table} = $data['data'][$column_index];
                  }
                }
              }
            }

            $update_data->update($update_queries);

            $data = $this->postUpdate($id, $data);

            $data['message'] = 'Record is successfully updated.';
        }

        $data['errors'] = $errors ? $errors : [];

        return $data;
  }

  public function edit($id){
    $data = [];

    $result = $this->model->where('id', $id)->first()->toArray();

    $data = $this->addFormData($result);

    // return view($this->view_folder . '.edit')->with($data);
  }

  public function create(){
    $data = $this->addFormData();

    // return view($this->view_folder . '.create')->with($data);
  }

  public function addShowData($data = []){
    return $data;
  }

  public function show($id){
    $data = [];
    $data['data'] = $this->model->where('id', $id)->first();
    $data = $this->addShowData($data);

    // if($this->request->isXmlHttpRequest()){
    //   return $data;
    // }

    return $data;
    // return view($this->view_folder . '.show')->with($data);
  }

  public function destroy( $id ){
    $data = [];

    if($this->auth){
      $this->model->where('deleted_by', 1);
    }

    $this->model->where('id', $id )->update([ 'deleted_at' => date('Y-m-d h:i:s') ]);

    $data = $this->model->where('id', $id)->first();

    return $data;
  }

  public function index(){
    if(empty($this->view_folder)){
      $this->view_folder = $this->table;
    }

    $data = $this->getList();

    $render_view = [
      'data' => $data
    ];
    // dd($this->view_folder . '.index');exit;

    // if($this->request->isXmlHttpRequest()){
    //   return json_encode($render_view);
    // }

    return json_encode($render_view);
    // return view($this->view_folder . '.index')->with($render_view);
  }
}
