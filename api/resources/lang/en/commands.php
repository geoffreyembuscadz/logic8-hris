<?php

return [
  'make_crud' => [
    'controller_created'  => ':controller has successfully created and located at app\Http\Controllers.',
    'language_created'    => 'Language file for :language has successfully created.',
    'model_created'       => ':model has successfully created and located at app\Http\Models.',
    'migration_created'   => 'Migration file for :class has been successfully created and location at database/migrations.',
    'view_created'        => 'Resource files has been successfully created at :destination .'
  ]
];
