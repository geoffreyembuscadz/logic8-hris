<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
        $table->increments('id');

        $table->string('name');

        $table->text('description');

        $table->decimal('srp', 4);

        $table->string('created_by', 100);

        $table->string('updated_by', 100)->nullable();

        $table->string('deleted_by', 100)->nullable();

        $table->softDeletes();

        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
