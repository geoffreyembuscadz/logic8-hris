<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class CategoryController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'created_at' ];

	public $model = 'CategoryModel';

	public $rules = [ 'name' => 'required' ];

	public $table = 'category';

	public function addListData($data = []){
        return $data;
    }

    public function addFormData($data = []){
        return $data;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }
}
